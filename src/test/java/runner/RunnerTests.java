package runner;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"src/main/resources/positive tests.feature", "src/main/resources/negative tests.feature"},
        glue = "stepdefenitions"
)
public class RunnerTests {
}
