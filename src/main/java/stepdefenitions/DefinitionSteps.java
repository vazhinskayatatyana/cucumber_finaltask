package stepdefenitions;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import manager.PageFactoryManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.*;

import static io.github.bonigarcia.wdm.WebDriverManager.chromedriver;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DefinitionSteps {

    WebDriver driver;
    PageFactoryManager pageFactoryManager;
    HomePage homePage;
    SearchResultPage searchResultPage;
    ShoppingCartPage shoppingCartPage;
    RegistrationPage registrationPage;
    ProductPage productPage;

    private static final long DEFAULT_TIMEOUT = 60;

    @Before
    public void testsSetUp() {
        chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        pageFactoryManager = new PageFactoryManager(driver);
    }

    @Given("User opens {string} page")
    public void openPage(final String url) {
        homePage = pageFactoryManager.getHomePage();
        homePage.openHomePage(url);
    }

    @And("User checks search field visibility")
    public void checksSearchFieldVisibility() {
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        homePage.isSearchFieldVisible();
    }

    @And("User checks header visibility")
    public void checksHeaderVisibility() {
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        homePage.isHeaderVisible();
    }

    @And("User checks preferences button visibility")
    public void checksPreferencesButtonVisibility() {
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        homePage.isPreferencesButtonVisible();
    }

    @And("User checks cart icon visibility")
    public void checksCartIconVisibility() {
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        homePage.isCartIconVisible();
    }

    @And("User checks My account menu visibility")
    public void checksMyAccountMenuVisibility() {
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        homePage.isMyAccountMenuVisible();
    }

    @And("User checks wishlist visibility")
    public void checksWishlistVisibility() {
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        homePage.isWishlistVisible();
    }

    @Then("User checks footer visibility")
    public void checksFooterVisibility() {
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        homePage.isFooterVisible();
    }

    @And("User clicks preferences button")
    public void clickPreferencesButton() {
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        homePage.isFooterVisible();
        homePage.isPreferencesButtonVisible();
        homePage.clickPreferenceButton();
    }

    @When("User makes search by keyword {string}")
    public void enterKeywordToSearchField(final String keyword) {
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        homePage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        homePage.waitVisibilityOfElement(DEFAULT_TIMEOUT, homePage.getSearchField());
        homePage.enterTextToSearchField(keyword);
    }

    @And("User clicks search button")
    public void clicksSearchButton() {
        homePage.clickSearchButton();
    }

    @Then("User checks that product in the search results {string}")
    public void checksTheSearchResults(final String keyword) {
        searchResultPage = pageFactoryManager.getSearchResultPage();
        searchResultPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertTrue(searchResultPage.getSearchResultText().contains(keyword));
    }

    @And("User clicks product type button")
    public void clicksProductTypeButton() {
        searchResultPage = pageFactoryManager.getSearchResultPage();
        searchResultPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        searchResultPage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        searchResultPage.clickProductTypeButton();
    }

    @And("User selects product type")
    public void selectedProductType() {
        searchResultPage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        searchResultPage.selectProductType();
        searchResultPage.waitVisibilityOfElement(DEFAULT_TIMEOUT,searchResultPage.getProductTypeButton());
        searchResultPage.waitVisibilityOfElement(DEFAULT_TIMEOUT,searchResultPage.getSelectedProductTypePopUp());
        searchResultPage.clickProductTypeButtonClose();
    }

    @Then("User checks that product in the search results is selected product type{string}")
    public void checksProductTypeIsSelected(final String productType) {
        searchResultPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        searchResultPage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        searchResultPage.waitVisibilityOfElements(DEFAULT_TIMEOUT, searchResultPage.getNameOfProducts());
        for (WebElement product : searchResultPage.getNameOfProducts()) {
            assertTrue(product.getText().contains(productType));
        }
    }
    @After
    public void tearDown() {
        driver.close();
    }

    @And("User clicks wish list on first product")
    public void clicksWishListIcon() {
        searchResultPage = pageFactoryManager.getSearchResultPage();
        searchResultPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        searchResultPage.clickWishListOnFirstProduct();
    }

    @Then("User checks that amount of products in wish list are {string}")
    public void checksThatAmountOfProductsInWishList(final String amountOfProducts) {
         searchResultPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
         searchResultPage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
         searchResultPage.waitVisibilityOfElement(DEFAULT_TIMEOUT,searchResultPage.getWishListButton());
         searchResultPage.isWishlistVisible();
         searchResultPage.clickWishListButton();
         searchResultPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
         searchResultPage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
         searchResultPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, searchResultPage.getWishListProductsCount());
         assertEquals(searchResultPage.getAmountOfProductsInWishList(), amountOfProducts);
    }

    @When("User selects country")
    public void userSelectedCountryByShopIn() {
            homePage.waitVisibilityOfElement(DEFAULT_TIMEOUT, homePage.getIsSelectCountryPopUp());
            homePage.isSelectCountryPopUpVisibility();
            homePage.clickCountryInPreferences();
            homePage.selectCountryInPreferences();
            homePage.clickUpdatePreferencesButton();
        }

    @When("User makes search by product code {string}")
    public void enterProductCodeToSearchField(final String productCode) {
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        homePage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        homePage.waitVisibilityOfElement(DEFAULT_TIMEOUT, homePage.getSearchField());
        homePage.enterTextToSearchField(productCode);
    }

    @And("User clicks add to bag on selected product")
    public void AddToBagOnSelectedProduct() {
        productPage = pageFactoryManager.getProductPage();
        productPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        productPage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        productPage.waitVisibilityOfElement(DEFAULT_TIMEOUT,productPage.getAddToCartButton());
        productPage.clickAddToCartButton();
    }

    @Then("User checks that amount of products in cart are {string}")
    public void checkAmountOfProductsInCart(final String amountOfProducts) {
        shoppingCartPage = pageFactoryManager.getShoppingCartPage();
        shoppingCartPage.waitVisibilityOfElement(DEFAULT_TIMEOUT,shoppingCartPage.getHeaderCartPopUp());
        assertTrue(shoppingCartPage.getCartHeaderText().contains(amountOfProducts));
    }

    @Then("User checks selected country in preferences by country {string}")
    public void userChecksSelectedCountryInPreferences(final String country) {
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        homePage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        homePage.isFooterVisible();
        homePage.isPreferencesButtonVisible();
        homePage.clickPreferenceButton();
        homePage.waitVisibilityOfElement(DEFAULT_TIMEOUT, homePage.getIsSelectCountryPopUp());
        homePage.isSelectCountryPopUpVisibility();
        assertTrue(homePage.getShopInText().contains(country));
    }

    @And("User clicks My account menu")
    public void clickMyAccountMenuButton() {
        homePage.waitVisibilityOfElement(DEFAULT_TIMEOUT,homePage.getMyAccountMenuButton());
        homePage.clickMyAccountMenuButton();
    }

    @When("User clicks Join button")
    public void clickJoinButton() {
        registrationPage = pageFactoryManager.getRegistrationPage();
        registrationPage.waitVisibilityOfElement(DEFAULT_TIMEOUT,registrationPage.getJoinButton());
        registrationPage.clickJoinButton();
    }

    @And("User enters email {string} into the field Email address")
    public void enterEmailEmailInRegistrationForm(final String email) {
        registrationPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        registrationPage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        registrationPage.waitVisibilityOfElement(DEFAULT_TIMEOUT,registrationPage.getEmailField());
        registrationPage.isEmailFieldVisibility();
        registrationPage.enterTextToEmailField(email);
    }

    @And("User enters first name {string} into the field First name")
    public void enterFirstNameInRegistrationForm(final String firstName) {
        registrationPage.waitVisibilityOfElement(DEFAULT_TIMEOUT,registrationPage.getFirstNameField());
        registrationPage.isFirstNameFieldVisibility();
        registrationPage.enterTextToFirstNameField(firstName);
    }

    @And("User enters last name {string} into the field Last name")
    public void enterLastNameInRegistrationForm(final String lastName) {
        registrationPage.enterTextToLastNameField(lastName);
    }

    @And("User enters password {string} into the field Password")
    public void enterPasswordInRegistrationForm(final String password) {
        registrationPage.enterTextToPasswordField(password);
    }

    @And("User clicks Join Asos button")
    public void clickJoinAsosButton() {
        registrationPage.clickJoinAsosButton();
    }

    @Then("User sees a message Birthday Error {string}")
    public void checkErrorMessage(final String errorMessage) {
        assertTrue(registrationPage.getTextBirthdayErrorMessage().contains(errorMessage));
    }

    @And("Selects day, month, year in the date of birth field")
    public void selectsDdMonthYearInRegistrationForm() {
        registrationPage.clickSelectBirthDay();
        registrationPage.clickDayOfBirth();
        registrationPage.clickSelectBirthMonth();
        registrationPage.clickMonthOfBirth();
        registrationPage.clickSelectBirthYear();
        registrationPage.clickYearOfBirth();
    }

    @Then("User sees a message Email error {string}")
    public void checkEmailErrorMessage(final String errorMessage) {
        assertTrue(registrationPage.getTextEmailErrorMessage().contains(errorMessage));
    }

    @Then("User sees a message Password error {string}")
    public void checkPasswordErrorMessage(final String errorMessage) {
        assertTrue(registrationPage.getPasswordErrorMessage().contains(errorMessage));
    }

    @And("User clicks checkout for buying product")
    public void сlickCheckoutForBuyingProduct() {
        shoppingCartPage = pageFactoryManager.getShoppingCartPage();
        shoppingCartPage.waitVisibilityOfElement(DEFAULT_TIMEOUT,shoppingCartPage.getHeaderCartPopUp());
        shoppingCartPage.clickСheckoutButton();
    }

    @Then("User sees a registration form")
    public void checkRegistrationForm() {
      registrationPage = pageFactoryManager.getRegistrationPage();
      registrationPage.waitVisibilityOfElement(DEFAULT_TIMEOUT,registrationPage.getRegistrationForm());
      registrationPage.isRegistrationFormVisibility();
    }
}


