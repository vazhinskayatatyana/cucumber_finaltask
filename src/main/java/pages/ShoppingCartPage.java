package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class ShoppingCartPage extends BasePage{

    @FindBy(xpath = "//div[@data-test-id='miniBagHeader']")
    private WebElement headerCartPopUp;

    @FindBy(xpath = "//span[@data-test-id='miniBagItemCount']")
    private WebElement cartHeader;

    @FindBy(xpath = "//span[@class='_1M-cSy1']")
    private List<WebElement> checkoutButton;

    public WebElement getHeaderCartPopUp(){
        return headerCartPopUp;
    }

    public String getCartHeaderText(){
        return cartHeader.getText();
    }

    public ShoppingCartPage(WebDriver driver) {
        super(driver);
    }

    public void clickСheckoutButton(){checkoutButton.get(1).click();}
}
