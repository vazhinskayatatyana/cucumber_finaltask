package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class RegistrationPage extends BasePage{

    @FindBy(xpath = "//a[@data-testid='signup-link']")
    private WebElement joinButton;

    @FindBy(xpath = "//input[@class='qa-email-textbox']")
    public WebElement emailField;

    @FindBy(xpath = "//input[@class='qa-firstname-textbox']")
    public WebElement firstNameField;

    @FindBy(xpath = "//input[@class='qa-lastname-textbox']")
    public WebElement lastNameField;

    @FindBy(xpath = "//input[@class='qa-password-textbox']")
    public WebElement passwordField;

    @FindBy(xpath = "//input[@id='register']")
    public WebElement joinAsosButton;

    @FindBy(xpath = "//span[@id='BirthYear-error']")
    public WebElement birthdayErrorMessage;

    @FindBy(xpath = "//select[@class='qa-birthday-textbox']")
    public WebElement selectBirthday;

    @FindBy(xpath = "//option[@value='1'and contains(text(),'1')]")
    public WebElement selectDayOfBirth;

    @FindBy(xpath = "//select[@class='qa-birthmonth-textbox']")
    public WebElement selectBirthMonth;

    @FindBy(xpath = "//option[@value='1'and contains(text(),'January')]")
    public WebElement selectMonthOfBirth;

    @FindBy(xpath = "//select[@class='qa-birthyear-textbox']")
    public WebElement selectBirthYear;

    @FindBy(xpath = "//option[@value='2006'and contains(text(),'2006')]")
    public WebElement selectYearOfBirth;

    @FindBy(xpath = "//span[@id='Email-error']")
    public WebElement emailErrorMessage;

    @FindBy(xpath = "//span[@id='Password-error']")
    public WebElement passwordErrorMessage;

    @FindBy(xpath = "//div[@class='container signin-container']" )
    public WebElement registrationForm;

    public void clickJoinButton(){joinButton.click();}

    public WebElement getJoinButton(){return joinButton;};

    public RegistrationPage(WebDriver driver) {
        super(driver);
    }

    public WebElement getEmailField(){return emailField;}

    public boolean isEmailFieldVisibility(){return emailField.isDisplayed();}

    public void enterTextToEmailField(final String email) {
        emailField.clear();
        emailField.sendKeys(email);
    }

    public WebElement getFirstNameField(){return firstNameField;}

    public boolean isFirstNameFieldVisibility(){return firstNameField.isDisplayed();}

    public void enterTextToFirstNameField(final String firstName) {
        firstNameField.clear();
        firstNameField.sendKeys(firstName);
    }

    public void enterTextToLastNameField(final String lastName) {
        lastNameField.clear();
        lastNameField.sendKeys(lastName);
    }

    public void enterTextToPasswordField(final String password) {
        passwordField.clear();
        passwordField.sendKeys(password);
    }

    public void clickJoinAsosButton(){
        joinAsosButton.click();
    }

    public String getTextBirthdayErrorMessage(){
        return birthdayErrorMessage.getText();
    }

    public void clickSelectBirthDay(){
        selectBirthday.click();
    }

    public void clickDayOfBirth(){
        selectDayOfBirth.click();
    }

    public void clickSelectBirthMonth(){
        selectBirthMonth.click();
    }

    public void clickMonthOfBirth(){
        selectMonthOfBirth.click();
    }

    public void clickSelectBirthYear(){
        selectBirthMonth.click();
    }

    public void clickYearOfBirth(){
        selectMonthOfBirth.click();
    }

    public String getTextEmailErrorMessage(){
        return emailErrorMessage.getText();
    }

    public String getPasswordErrorMessage(){
        return passwordErrorMessage.getText();
    }

    public WebElement getRegistrationForm(){
        return registrationForm;
    }

    public boolean isRegistrationFormVisibility(){
        return registrationForm.isDisplayed();
    }
}
