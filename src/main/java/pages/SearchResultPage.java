package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class SearchResultPage extends BasePage {

    @FindBy(xpath = "//div[@data-auto-id='productTileDescription']")
    private List<WebElement> productNames;

    @FindBy(xpath = "//p[@class='vp-JnyG']")
    private WebElement searchResultText;

    @FindBy(xpath = "//div[@class='_2pwX7b9' and contains(text(),'Product Type')]")
    private WebElement productTypeButtonOpen;

    @FindBy(xpath = "//button[@class='_1om7l06 u0n_ENG']")
    private WebElement productTypeButtonClose;

    @FindBy (xpath = "//div[@class='kx2nDmW' and contains(text(),'Bags')]")
    private WebElement selectedProductType;

    @FindBy(xpath = "//span[@class='_30BqGyh']")
    private List<WebElement> wishListIcons;

    @FindBy(xpath = "//a[@data-testid='savedItemsIcon']")
    private WebElement wishListButton;

    @FindBy(xpath = "//div[@class='itemCount_ftSVY']")
    private WebElement wishListProductsCount;

    @FindBy(xpath = "//div[@class='_2EAcS_V _2H7teJE']")
    private WebElement selectProductTypePopUp;

    public SearchResultPage(WebDriver driver) {
        super(driver);
    }

    public List<WebElement> getNameOfProducts() {
        return productNames;
    }

    public String getSearchResultText(){
        return searchResultText.getText();
    }

    public void clickProductTypeButton(){
        productTypeButtonOpen.click();
    }
    public void clickProductTypeButtonClose(){
        productTypeButtonClose.click();
    }

    public void selectProductType(){
        selectedProductType.click();
    }

    public WebElement getProductTypeButton() {
        return productTypeButtonClose;
    }

    public void clickWishListOnFirstProduct() {
        wishListIcons.get(0).click();
    }

    public WebElement getWishListButton(){
        return wishListButton;
    }

    public void clickWishListButton() {
        wishListButton.click();
    }

    public boolean isWishlistVisible(){
        return wishListButton.isDisplayed();
    }

    public String getAmountOfProductsInWishList(){
        return wishListProductsCount.getText();
    }

    public WebElement getWishListProductsCount() {
        return wishListProductsCount;
    }

    public WebElement getSelectedProductTypePopUp() {
        return selectProductTypePopUp;
    }
}
