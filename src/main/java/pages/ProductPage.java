package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ProductPage extends BasePage{

    @FindBy(xpath = "//button[@data-test-id='add-button']")
    private WebElement addToCartButton;

    public WebElement getAddToCartButton(){
        return addToCartButton;
    }

    public void clickAddToCartButton(){
        addToCartButton.click();
    }

    public ProductPage(WebDriver driver) {
        super(driver);
    }
}
