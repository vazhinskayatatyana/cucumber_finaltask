package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class HomePage extends BasePage{

    @FindBy (xpath = "//input[@id='chrome-search']")
    private WebElement searchField;

    @FindBy (xpath = "//button[@data-testid='search-button-inline']")
    private WebElement searchButton;

    @FindBy(xpath = "//header")
    private WebElement header;

    @FindBy(xpath = "//footer")
    private WebElement footer;

    @FindBy(xpath = "//button[@data-testid='country-selector-btn']")
    private List<WebElement> preferencesButton;

    @FindBy(xpath = "//a[@data-testid='miniBagIcon']")
    private WebElement cartIcon;

    @FindBy(xpath = "//button[@data-testid='myAccountIcon']")
    private WebElement myAccountMenu;

    @FindBy(xpath = "//a[@data-testid='savedItemsIcon']")
    private WebElement wishListButton;

    @FindBy(xpath = "//select[@class='_2CHxSwY _3ps0adE']")
    private List<WebElement> selectCountryInPreferences;

    @FindBy(xpath = "//option[@value='UA']")
    private WebElement chooseCountry;

    @FindBy(xpath = "//h1[@class='_3Dnbc1A _2h9sodS']")
    private WebElement selectCountryPopUp;

    @FindBy(xpath = "//button[@data-testid='save-country-button']")
    private WebElement updatePreferencesButton;

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void openHomePage(String url) {
        driver.get(url);
    }

    public boolean isSearchFieldVisible(){
        return searchField.isDisplayed();
    }

    public void enterTextToSearchField(final String searchText) {
        searchField.clear();
        searchField.sendKeys(searchText);
    }

    public void clickSearchButton () {
        searchButton.click();
    }

    public boolean isHeaderVisible() {
        return header.isDisplayed();}

    public boolean isFooterVisible() {
        return footer.isDisplayed();
    }

    public WebElement getSearchField() {
        return searchField;
    }

    public boolean isPreferencesButtonVisible(){return preferencesButton.get(0).isDisplayed();}

    public void clickPreferenceButton(){preferencesButton.get(0).click();}

    public boolean isCartIconVisible(){
        return cartIcon.isDisplayed();
    }

    public boolean isMyAccountMenuVisible(){
        return myAccountMenu.isDisplayed();
    }

    public WebElement getMyAccountMenuButton() {return myAccountMenu;}

    public void clickMyAccountMenuButton() {myAccountMenu.click();}

    public boolean isWishlistVisible(){
        return wishListButton.isDisplayed();
    }

    public boolean isSelectCountryPopUpVisibility(){
        return selectCountryPopUp.isDisplayed();
    }

    public WebElement getIsSelectCountryPopUp(){
        return selectCountryPopUp;
    }

    public void clickCountryInPreferences(){
        selectCountryInPreferences.get(0).click();
    }
    public void selectCountryInPreferences(){
        chooseCountry.click();
    }

    public void clickUpdatePreferencesButton(){
        updatePreferencesButton.click();
    }

    public String getShopInText(){return selectCountryInPreferences.get(0).getText();}
}
