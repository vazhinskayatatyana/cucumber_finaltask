Feature: Negative tests
  As a user
  I want to test main functionality in case of incorrect behavior
  So that I can be sure that site will show me proper Error message

  Scenario Outline: Check that the "Date of birth" field is required in the registration form
    Given User opens '<homePage>' page
    And User checks header visibility
    And User checks My account menu visibility
    And User clicks My account menu
    When User clicks Join button
    And User enters email '<email>' into the field Email address
    And User enters first name '<first name>' into the field First name
    And User enters last name '<last name>' into the field Last name
    And User enters password '<password>' into the field Password
    And User clicks Join Asos button
    Then User sees a message Birthday Error '<message>'

    Examples:
      | homePage                    | message                       |email         |first name|last name|password   |
      | https://www.asos.com/women/ | Enter your full date of birth |123@gmail.com |Tanya     |Tanya    |1234567890 |

  Scenario Outline: Check that the "Email address" field is required in the registration form
    Given User opens '<homePage>' page
    And User checks header visibility
    And User checks My account menu visibility
    And User clicks My account menu
    When User clicks Join button
    And User enters first name '<first name>' into the field First name
    And User enters last name '<last name>' into the field Last name
    And User enters password '<password>' into the field Password
    And Selects day, month, year in the date of birth field
    And User clicks Join Asos button
    Then User sees a message Email error '<message>'

    Examples:
      | homePage                    | message                                |first name|last name|password   |
      | https://www.asos.com/women/ | Oops! You need to type your email here |Tanya     |Tanya    |1234567890 |

  Scenario Outline: Check that the "Password" field is required in the registration form
    Given User opens '<homePage>' page
    And User checks header visibility
    And User checks My account menu visibility
    And User clicks My account menu
    When User clicks Join button
    And User enters email '<email>' into the field Email address
    And User enters first name '<first name>' into the field First name
    And User enters last name '<last name>' into the field Last name
    And Selects day, month, year in the date of birth field
    And User clicks Join Asos button
    Then User sees a message Password error '<message>'

    Examples:
      | homePage                    | message                       |email         |first name|last name|
      | https://www.asos.com/women/ | Hey, we need a password here |123@gmail.com  |Tanya     |Tanya    |

  Scenario Outline: Check that the registration is required for buying the product
    Given User opens '<homePage>' page
    And User checks search field visibility
    When User makes search by product code '<product code>'
    And User clicks search button
    And User clicks add to bag on selected product
    And User clicks checkout for buying product
    Then User sees a registration form

    Examples:
      | homePage                            | product code   |
      | https://www.asos.com/women/         | 113028650      |