Feature: Positive tests
  As a user
  I want to test all main site functionality
  So that I can be sure that site works correctly

  Scenario Outline: Check main components on Home Page
    Given User opens '<homePage>' page
    And User checks header visibility
    And User checks preferences button visibility
    And User checks search field visibility
    And User checks cart icon visibility
    And User checks My account menu visibility
    And User checks wishlist visibility
    Then User checks footer visibility

    Examples:
      | homePage                    |
      | https://www.asos.com/women/ |

  Scenario Outline: Check select country in the preferences
    Given User opens '<homePage>' page
    And User checks header visibility
    And User checks preferences button visibility
    And User clicks preferences button
    When User selects country
    Then User checks selected country in preferences by country '<country>'

    Examples:
      | homePage                    |country   |
      | https://www.asos.com/women/ |Ukraine   |

  Scenario Outline: Check search the product
    Given User opens '<homePage>' page
    And User checks search field visibility
    When User makes search by keyword '<keyword>'
    And User clicks search button
    Then User checks that product in the search results '<keyword>'

    Examples:
      | homePage                    | keyword |
      | https://www.asos.com/women/ | Shoes   |

  Scenario Outline: Check the selection of product type
    Given User opens '<homePage>' page
    And User checks search field visibility
    When User makes search by keyword '<keyword>'
    And User clicks search button
    And User clicks product type button
    And User selects product type
    Then User checks that product in the search results is selected product type'<product type>'

    Examples:
      | homePage                    | keyword | product type |
      | https://www.asos.com/women/ | Shoes   | bag          |

  Scenario Outline: Check add product to wishlist
    Given User opens '<homePage>' page
    And User checks search field visibility
    When User makes search by keyword '<keyword>'
    And User clicks search button
    And User clicks wish list on first product
    Then User checks that amount of products in wish list are '<amountOfProducts>'

    Examples:
      | homePage                            | keyword | amountOfProducts |
      | https://www.asos.com/women/         | Shoes   | 1 item           |

  Scenario Outline: Check add product to cart
    Given User opens '<homePage>' page
    And User checks search field visibility
    When User makes search by product code '<product code>'
    And User clicks search button
    And User clicks add to bag on selected product
    Then User checks that amount of products in cart are '<amountOfProducts>'

    Examples:
      | homePage                            | product code   | amountOfProducts |
      | https://www.asos.com/women/         | 113028650      | 1 item           |

